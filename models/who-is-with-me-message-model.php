<?php

/**
 * The Who Is With Me Message model
 */


// Abort if called directly
// NOTE TO SELF: Find a more graceful way to handle this, dying is so harsh
if ( ! defined( 'WPINC' ) ){
	die();
}

if ( ! class_exists( 'Who_Is_With_Me_Message_Model' ) ) {

	/**
	 * The Messages Model for the Who Is With Me Plugin
	 * 
	 * @since  1.0.0
	 */
	class Who_Is_With_Me_Message_Model {

		// class variables all set to private
		private $wpdb;
		private $tracker_table     = '';
		private $charset_collate   = '';
		private $message           = '';
		private $threshold         = 1;

		/**
		 * The class constructor
		 * 
		 * @since 1.0.0
		 */
		public function __construct() {

			// make reference to the global $wpdb available to Who_Is_With_Me_Event_Model object
			global $wpdb;
			$this->wpdb = &$wpdb;

			// set class variable values
			$this->set_tracker_table( $this->wpdb->prefix . 'ca_tracker_messages' );
			$this->set_charset_collate( $this->wpdb->get_charset_collate() );
		}

		/*
		 * Getters and setters go here
		 *
		 * NOTE TO SELF: I should probably define all getters and setters, but too lazy right now 
		 */

		private function set_tracker_table( $t ) {
			$this->tracker_table = $t;
		}

		private function get_tracker_table() {
			return $this->tracker_table;
		}

		private function set_charset_collate( $t ) {
			$this->charset_collate = $t;
		}

		private function get_charset_collate() {
			return $this->charset_collate;
		}

		/**
		 * Get the message for the event
		 *
		 * @since  1.0.0
		 * @return string
		 */
		public function get_message( $c ) {

			return $this->wpdb->get_var( "SELECT message FROM " . $this->get_tracker_table() . " WHERE threshold <= $c ORDER BY threshold DESC LIMIT 1" );
		}

		/* End getters and setters */


		/**
		 * Create tables on plugin activation
		 * 
		 * @since 1.0.0
		 */
		public function create_tables() {

			$table_name = $this->get_tracker_table();
			$collate = $this->get_charset_collate();

			$sql = "CREATE TABLE $table_name (
				id mediumint(9) NOT NULL AUTO_INCREMENT,
				message varchar(255) DEFAULT '' NOT NULL,
				threshold tinyint DEFAULT 0 NOT NULL,
				UNIQUE KEY id (id)
			) $collate";

			dbDelta( $sql );
		}

		/**
		 * Load default data
		 * 
		 * @since 1.0.0
		 */
		public function load_default_data() {
			$table_name = $this->get_tracker_table();

			$message_cnt = $this->wpdb->get_var( "SELECT COUNT(*) FROM $table_name" );

			if ( $message_cnt == 0 ) {
				$messages = array(
					array( 'message' => "You are an intrepid explorer, braving the wilds of this lonely blog.", 'threshold' => 1 ),
					array( 'message' => "Alas! A fellow journeyman (or woman) is exploring this site with you.", 'threshold' => 2 ),
					array( 'message' => "There are at least 2 other adventurers in this pristine site. Obviously, it's getting too crowded around here.", 'threshold' => 3 ),
				);

				foreach ( $messages as $message ) {
					$this->wpdb->insert( 
						$table_name,
						$message
					);
				}
			}
		}

	} // end Who_Is_With_Me_Message_Model
}