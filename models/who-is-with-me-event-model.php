<?php

/**
 * The Who Is With Me event model
 */


// Abort if called directly
// NOTE TO SELF: Find a more graceful way to handle this, dying is so harsh
if ( ! defined( 'WPINC' ) ){
	die();
}

if ( ! class_exists( 'Who_Is_With_Me_Event_Model' ) ) {

	/**
	 * The Event Model for the Who Is With Me Plugin
	 *
	 * @since  1.0.0
	 */
	class Who_Is_With_Me_Event_Model {

		// class variables all set to private
		private $wpdb;
		private $tracker_table     = '';
		private $charset_collate   = '';

		/**
		 * The class constructor
		 * 
		 * @since 1.0.0
		 */
		public function __construct() {

			// make reference to the global $wpdb available to Who_Is_With_Me_Event_Model object
			global $wpdb;
			$this->wpdb = &$wpdb;

			// set class variable values
			$this->set_tracker_table( $this->wpdb->prefix . 'ca_tracker_events' );
			$this->set_charset_collate( $this->wpdb->get_charset_collate() );
		}

		/*
		 * Getters and setters go here
		 *
		 * NOTE TO SELF: I should probably define all getters and setters, but too lazy right now 
		 */
	
		private function set_tracker_table( $t ) {
			$this->tracker_table = $t;
		}

		private function get_tracker_table() {
			return $this->tracker_table;
		}

		private function set_charset_collate( $t ) {
			$this->charset_collate = $t;
		}

		private function get_charset_collate() {
			return $this->charset_collate;
		}

		/**
		 * Saves event to database
		 * @param  string $ip     IP address
		 * @param  string $action Event action
		 * @param  string $url    URL for event
		 * @return boolean        Result of save request
		 */
		public function save_event( $ip = null, $action = null, $url = null ) {

			// all parameters are required
			if ( is_null( $ip ) || is_null( $action ) || is_null( $url ) ) {
				return false;
			}

			// save to database
			// NOTE TO SELF: Do I need to sanitize variables or does insert() take care of it for me?
			$result = $this->wpdb->insert( $this->get_tracker_table(), array(
				'ip'     => $ip,
				'action' => $action,
				'url'    => $url,
				'time'   => current_time('mysql')
			));

			if ( 0 == $result ) {
				return false;
			} else {
				return true;
			}
		}

		/* End getters and setters */

		/**
		 * Create tables on plugin activation
		 * 
		 * @since 1.0.0
		 */
		public function create_tables() {

			$table_name = $this->get_tracker_table();
			$collate = $this->get_charset_collate();

			$sql = "CREATE TABLE $table_name (
				id mediumint(9) NOT NULL AUTO_INCREMENT,
				ip varchar(16) DEFAULT '0.0.0.0' NOT NULL,
				url varchar(255),
				action varchar(16) DEFAULT 'view' NOT NULL,
				time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
				UNIQUE KEY id (id)
			) $collate";

			dbDelta( $sql );
		}

		/**
		 * Get total number of events - not currently used
		 *
		 * @since  1.0.0
		 * @return int
		 */
		public function get_total_num_recent_events() {
			
			return $this->wpdb->get_var( "SELECT COUNT(*) FROM " . $this->get_tracker_table() );
		}

		/**
		 * Get total number of recent events on requested page
		 *
		 * @param  string $p  URL of page to get recent events for
		 */
		public function get_unique_num_events_on_page( $p ) {
			// stub function
		}

		/**
		 * Get total number of recent events since elapsed time
		 *
		 * @param  string $t  Amount of elapsed time to get total events, ex. 5min
		 * @since  1.0.0
		 * @return int
		 */
		public function get_unique_num_events_since( $t ) {

			return $this->wpdb->get_var( "SELECT COUNT(DISTINCT(ip)) FROM " . $this->get_tracker_table() . " WHERE time >= NOW() - INTERVAL $t" );
		}


	} // end Who_Is_With_Me_Event_Model
}