<?php

/*
Plugin Name:  Who is With Me
Description:  Reveals how many people are currently viewing this website.
Version:      1.1.0
Author:       Chris Amelung
Author URI:   https://chrisamelung.com
License:      GPL2
*/


// Abort if called directly
// NOTE TO SELF: Find a more graceful way to handle this, dying is so harsh
if ( ! defined( 'WPINC' ) ){
	die();
}

if ( ! class_exists( 'Who_Is_With_Me' ) ) {

	/**
	 * The main plugin controller - designed as a Singleton
	 * 
	 * @since  1.0.0
	 */
	class Who_Is_With_Me {

		// class variables all set to private
		private static $instance;
		private $directories = array();
		private $event_model;
		private $message_model;

		/**
		 * Instantiate a singleton of the plugin
		 *
		 * @since  1.0.0
		 * @static
		 * @return Instance
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * The class constructor - set to private to enforce Singleton design
		 * 
		 * @since 1.0.0
		 * @access private
		 */
		private function __construct() {

			// define directories
			$this->define_directories();

			// load required files
			$this->load_plugin_files();

			// create database objects
			// NOTE TO SELF: Database design for this plugin is incredibly simplistic - will need to expand and normalize if ever build out features
			$this->event_model = new Who_Is_With_Me_Event_Model();
			$this->message_model = new Who_Is_With_Me_Message_Model();
			
			$this->init();
		}

		/**
		 * Initialization of object with WordPress
		 * 
		 * @since 1.0.0
		 */
		public function init() {

			register_activation_hook( __FILE__, array( &$this, 'activate_me' ) );

			add_action( 'widgets_init', array( &$this, 'register_my_widget' ) );

			add_action( 'init', array( &$this, 'register_shortcodes' ) );

			add_action( 'template_redirect', array( &$this, 'track_visitor' ) );

		}

		/**
		 * Registers the widget
		 * 
		 * @since 1.0.0
		 */
		public function register_my_widget() { 
			register_widget( 'Who_Is_With_Me_Widget' ); 
		} 

		/**
		 * Registers the shortcodes
		 * 
		 * @since 1.0.0
		 */
		public function register_shortcodes() {
			add_shortcode( 'recent-posts', 'my_recent_posts' );
			add_shortcode( 'who-with-me', 'who_is_with_me' );
		}

		/**
		 * Track visitors
		 * 
		 * @since 1.0.0
		 */
		public function track_visitor() {
			
			// Don't track users managing the site
			if ( ! is_user_logged_in() ) {

				// get the headers if we can or else use the SERVER global
				if ( function_exists( 'apache_request_headers' ) ) {
					$headers = apache_request_headers();
				} else {
					$headers = $_SERVER;
				}
				
				// get the forwarded IP if it exists
				// NOTE: Why in the world is this not a core WordPress function
				if ( array_key_exists( 'X-Forwarded-For', $headers ) && filter_var( $headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
					$the_ip = $headers['X-Forwarded-For'];
				} elseif ( array_key_exists( 'HTTP_X_FORWARDED_FOR', $headers ) && filter_var( $headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 )
				) {
					$the_ip = $headers['HTTP_X_FORWARDED_FOR'];
				} else {
					$the_ip = filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 );
				}

				// get the url of the page a user viewed
				// NOTE: This is a future feature option - not currently used
				$url = get_bloginfo( 'url' ) . $_SERVER['REQUEST_URI'];

				// save event to database, report error if save fails
				// NOTE TO SELF: Learn the proper way to manage errors
				if ( ! $this->event_model->save_event( $the_ip, 'view', $url ) ) {
					error_log( "FAILED TO SAVE: $the_ip, view, $url" );
				}
			}
		}

		/**
		 * Create/verify tables and initialize if needed
		 * 
		 * @since 1.0.0
		 */
		public function activate_me() {

			// NOTE TO SELF: Need to verify tables exist before continuing
			$this->event_model->create_tables();

			$this->message_model->create_tables();
			$this->message_model->load_default_data();
		}

		/**
		 * Helper function to get directory path
		 * 
		 * @param  string $name directory name to locate
		 * @since  1.0.0
		 * @return string filesystem path to requested directory
		 */
		public function get_directory( $name ) {
			
			// NOTE TO SELF: Should sanitize variable to prevent directory crawls
			return $this->directories[ $name ];
		}


		/**
		 * Define the paths of each directory in the plugin
		 *
		 * @since  1.0.0
		 * @access private
		 */
		private function define_directories() {
			$plugin_dir = plugin_dir_path( __FILE__ );

			$this->directories = array(
				'plugin_root'    => $plugin_dir,
				'models'         => $plugin_dir . 'models/',
				'views'          => $plugin_dir . 'views/',
				'widgets'        => $plugin_dir . 'views/widgets/',
			);
		
		}

		/**
		 * Load the required files in the plugin
		 * 
		 * @since 1.0.0
		 * @access private
		 */
		private function load_plugin_files() {

			// need to do file check?
			// better solution than ABSPATH?
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

			require_once( $this->get_directory( 'models' ) . 'who-is-with-me-event-model.php' );

			require_once( $this->get_directory( 'models' ) . 'who-is-with-me-message-model.php' );

			require_once( $this->get_directory( 'widgets' ) . 'who-is-with-me-widget.php' );

			require_once( $this->get_directory( 'views' ) . 'shortcodes.php' );

		}


	} // end Who_Is_With_Me
}

// Load the plugin
$with_me_plugin = Who_Is_With_Me::get_instance();

