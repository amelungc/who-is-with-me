<?php

/*
 * Shortcode functions for Who Is With Me plugin
 */

/**
 * Display list of recent posts - tutorial function from https://www.smashingmagazine.com/2012/05/wordpress-shortcodes-complete-guide/
 * @since  1.1.0 
 * @return [string]
 */
function my_recent_posts() {
	query_posts( array( 'orderby' => 'date', 'order' => 'DESC', 'showposts' => 1 ) );
	if ( have_posts() ) {
		while ( have_posts() ) : the_post();
			$return_string = '<a href="' . get_permalink() . '">' . get_the_title() . '</a>';
		endwhile;
	}
	wp_reset_query();
	return $return_string;
}

/**
 * Returns Who Is With Me message based on number of recent visitors
 * @return [type] [description]
 */
function who_is_with_me() {
	$event_model = new Who_Is_With_Me_Event_Model();
	$message_model = new Who_Is_With_Me_Message_Model();

	// get the number of recent events
	$num_events = $event_model->get_unique_num_events_since( '1 HOUR' );
	$return_string = '<pre>' . esc_html( $message_model->get_message( $num_events ) ) . '</pre>';

	return $return_string;
}
