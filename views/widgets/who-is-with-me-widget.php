<?php

/*
Plugin Name:  Who is With Me Widget
Description:  Widget to reveal how many people are currently viewing this website.
Version:      1.0.0
Author:       Chris Amelung
Author URI:   https://chrisamelung.com
License:      GPL2
*/

// Abort if called directly
// NOTE TO SELF: Find a more graceful way to handle this, dying is so harsh
if ( ! defined( 'WPINC' ) ){
	die();
}

if ( ! class_exists( 'Who_Is_With_Me_Widget' ) )  {

	/**
	 * Widget class
	 *
	 * @since  1.0.0
	 */
	class Who_Is_With_Me_Widget extends WP_Widget {

		// class variables all set to private
		private $event_model;
		private $message_model;

		/**
		 * The class constructor
		 * 
		 * @since 1.0.0
		 */
		public function __construct() {

			parent::__construct(
				'Who_Is_With_Me_Widget',
				__( 'Who Is With Me Widget', 'text_domain' ),
				array( 'description' => __( 'Widget to show how many are also viewing the site.', 'text_domain' ), )
			);

			// create database objects
			// NOTE TO SELF: See if there's a way to reference these already created objects, inheritance perhaps? 			
			$this->event_model = new Who_Is_With_Me_Event_Model();
			$this->message_model = new Who_Is_With_Me_Message_Model();

		}

		/**
		 * Front-end display of widget
		 *
		 * @see WP_Widget::widget()
		 * 
		 * @param  array $args     Widget arguments
		 * @param  array $instance Saved values from database
		 * @since  1.0.0
		 */
		public function widget( $args, $instance ) {
			
			// get the number of recent events
			$num_events = $this->event_model->get_unique_num_events_since( '1 HOUR' );

			// display the wonder of this magical widget
			echo $args['before_widget'];
			if ( ! empty( $instance['title'] ) ) {
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args[ 'after_title' ];
			}
			echo '<p>' . esc_html( $this->message_model->get_message( $num_events ) ) . '</p>';
			// echo '<p style="text-align: right;"><a href="">What is this?</a></p>';
			echo $args['after_widget'];
		}

		/**
		 * Back-end widget form - a pointless feature for now, will expand later
		 *
		 * @see WP_Widget::form()
		 * 
		 * @param  array $instance Previously saved values from database
		 * @since  1.0.0
		 */
		public function form( $instance ) {
			if ( isset( $instance['title'] ) ) {
				$title = $instance['title'];
			} else {
				$title = __( 'New title', 'wpb_widget_domain' );
			}
			?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ) ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			<?php
		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @see WP_Widget::update()
		 *
		 * @param array $new_instance Values just sent to be saved.
		 * @param array $old_instance Previously saved values from database.
		 *
		 * @return array Updated safe values to be saved.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		
			return $instance;
		}

	} // end Who_Is_With_Me_Widget
}
