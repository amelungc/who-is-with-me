Who Is With Me
==============================

This plugin is an experiment.

The general idea is that it tracks visits and displays a message to all visitors that identifies how many people have viewed this site in the last hour.

The real usefulness of this plugin is that it has been a place for me to experiment. I have explored the following with this plugin:

- Using a singleton
- Additional experiementation with MVC design in WordPress
- Creating database tables and inserting/retrieving data
- Working with register_activation_hook()
- Creating and using a widget
- Creating and using a shortcode